class Rent < ApplicationRecord
  belongs_to :user
  belongs_to :address, optional: true
  has_many :rent_games
  # has_many :games, through: :rent_games
  before_save :update_total
  before_create :update_status

  def calculate_total
    rent_games.collect { |item| item.game.rentPrice * item.week }.sum
  end
  
  private
  
  def update_status
    self.status = 'In progress' if status == nil?
  end

  def update_total
    self.totalValue = calculate_total
  end
end
