class User < ApplicationRecord
  has_many :rents
  has_many :games
  has_many :addresses

  enum role: [:normal_user, :admin]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :trackable
end
