class Game < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :user
  belongs_to :category
  has_many :rent_games
  # has_many :rents, through: :rent_games
end
