function inputFileName() {
    var fullPath = document.getElementById('image').value;
    if (fullPath)
    {
        var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\'): fullPath.lastIndexOf('/'));
        var fileName = fullPath.substring(startIndex);
        if (fileName.indexOf('\\') === 0 || fileName.indexOf('/') === 0)
        {
            fileName = fileName.substring(1);
        }
        span = document.getElementById('file-name');
        document.getElementById('file-name').style.display = "block";
        span.innerHTML = fileName;
    }
}

function valorAluguel()
{
    var semana = document.getElementById('semana').value;
    var rentPrice = document.getElementById('rentPrice').value;
    span = document.getElementById('valor');
    span.innerHTML = Intl.NumberFormat('pt-br', {style: 'currency', currency: 'BRL'}).format(semana * rentPrice);
}