class CartsController < ApplicationController
  before_action :authenticate_user!

  def show
    @rent_games = current_rent.rent_games
  end
end
