class ApplicationController < ActionController::Base
    include Pundit
    protect_from_forgery with: :exception

    helper_method :current_rent

    def current_rent
        if session[:rent_id]
            Rent.find(session[:rent_id])
        else
            Rent.new
        end
    end

    before_action :configure_permitted_parameters, if: :devise_controller?

    protected

    def configure_permitted_parameters
        devise_parameter_sanitizer.permit(:sign_up) { |u| u.permit(:name, :email, :password, :remember_me)}
        devise_parameter_sanitizer.permit(:account_update) { |u| u.permit(:name, :email, :password, :remember_me)}
    end

    rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

    private 

    def user_not_authorized
        flash[:notice] = "Você não tem permissão para fazer esta ação."
        redirect_to(request.referrer || root_path)
    end
end
