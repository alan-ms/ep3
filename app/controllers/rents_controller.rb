class RentsController < ApplicationController
    before_action :set_rent, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!
    before_action :address_params, only: [:set_address]

    def index
      if current_user.role == 'admin'
        @rents = Rent.all.where(status: 'finish')
      end
      if current_user.role == 'normal_user'
        @rents = Rent.all.where(user_id: current_user.id).where(status: 'finish')
      end
    end

    def show
    end
  
    def new
      @rent = Rent.new
    end
  
    def edit
    end

    def set_address
      @rent = current_rent
      @rent.address = @address
      @rent.save
      redirect_to rents_confirmation_page_path
    end

    def rent_confirmation
      @rent = current_rent
      @rent.status = 'finish'

      respond_to do |format|
        @rent.rent_games.each do |item|
          item.game.status = false
          item.game.save
        end
        if @rent.save
          format.html { redirect_to root_path, notice: 'Aluguel realizado com sucesso!'}
          session.delete(:rent_id)
        end
      end
    end

    def confirmation_page
      @rent = current_rent
    end

    def addressToRent
      @addresses = current_user.addresses
    end

    def create
        @rent = Rent.new(rent_params)
    
        respond_to do |format|
            if @rent.save
                format.html { redirect_to @rent, notice: 'Rent was successfully created.' }
                format.json { render :show, status: :created, location: @rent }
            else
                format.html { render :new }
                format.json { render json: @rent.errors, status: :unprocessable_entity }
            end
        end
    end

    def update
        respond_to do |format|
            if @rent.update(rent_params)
                format.html { redirect_to @rent, notice: 'Rent was successfully updated.' }
                format.json { render :show, status: :ok, location: @rent }
            else
                format.html { render :edit }
                format.json { render json: @rent.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
        @rent.destroy
        respond_to do |format|
            format.html { redirect_to rents_url, notice: 'Rent was successfully destroyed.' }
            format.json { head :no_content }
        end
    end

    private

    def set_rent
        @rent = Rent.find(params[:id])
    end

    def rent_params
        params.require(:rent).permit(:time, :date, :totalValue)
    end

    def address_params
      @address = Address.find(params[:address_id])
    end

end


