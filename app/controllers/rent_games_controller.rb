class RentGamesController < ApplicationController
  # before_action :set_rent_game, only: [:show, :edit, :update, :destroy]
    before_action :address_params, only: [:set_address]
    before_action :authenticate_user!

    def create
      @rent = current_rent
      @rent_game = @rent.rent_games.new(game_params)
      @rent_game.subtotal = @rent_game.week * @rent_game.game.rentPrice
      @rent.user_id = current_user.id
      @rent.status = 'In progress'
      @rent.save
      session[:rent_id] = @rent.id
      redirect_to '/carts/'
    end

    def destroy
      @rent = current_rent
      @rent_game = @rent.rent_games.find(params[:id])
      @rent_game.destroy
      @rent_games = current_rent.rent_games
      if @rent_games.none?
        session.delete(:rent_id)
        @rent.destroy
      end
      redirect_to '/carts/'
    end

  private

    def rent_game_params
      params.require(:rent_game).permit(:game_id, :rent, :week, :subtotal)
    end

    def game_params
      params.require(:rent_game).permit(:week, :game_id)
    end

end
