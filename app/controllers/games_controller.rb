class GamesController < ApplicationController
    before_action :set_game, only: [:show, :edit, :update, :destroy]
    before_action :authenticate_user!, except: [:index]

    def index
        @games = Game.all.where(status: 'true')
    end

    def show
        @rent_games = current_rent.rent_games.new
    end

    def myGames
        @games = current_user.games.where(visible: true)
    end

    def new
      @game = Game.new
      @categories = Category.all
    end

    def edit
      @categories = Category.all
    end

    def create
        @game = Game.new(game_params)
        @game.user_id = current_user.id
        @game.status = true
        respond_to do |format|
            if @game.save
                format.html { redirect_to @game, notice: 'Game was successfully created.' }
                format.json { render :show, status: :created, location: @game }
            else
                format.html { render :new }
                format.json { render json: @game.errors, status: :unprocessable_entity }
            end
        end
    end

    def update
        respond_to do |format|
            if @game.update(game_params)
                format.html { redirect_to @game, notice: 'Game was successfully updated.' }
                format.json { render :show, status: :ok, location: @game }
            else
                format.html { render :edit }
                format.json { render json: @game.errors, status: :unprocessable_entity }
            end
        end
    end

    def destroy
      begin
        @game.destroy
        respond_to do |format|
          format.html { redirect_to games_url, notice: 'Jogo foi foi excluído com  sucesso!' }
          format.json { head :no_content }
        end
      rescue
        format.html { redirect_to games_url, notice: 'Ação não pode ser executada por referência do jogo em um alguel!' }
      end

    end

    private

    def set_game
        @game = Game.find(params[:id])
    end

    def game_params
        params.require(:game).permit(:name, :description, :rentPrice, :image, :category_id)
    end
end
