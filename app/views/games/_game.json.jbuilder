json.extract! game, :id, :name, :description, :rentPrice, :image, :status, :user_id, :category_id, :created_at, :updated_at
json.url game_url(game, format: :json)
