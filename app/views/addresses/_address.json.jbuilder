json.extract! address, :id, :address, :district, :city, :uf, :cep, :complement, :user_id, :created_at, :updated_at
json.url address_url(address, format: :json)
