json.extract! rent_game, :id, :game_id, :rent, :week, :subtotal, :created_at, :updated_at
json.url rent_game_url(rent_game, format: :json)
