Rails.application.routes.draw do
  post 'rents/rent_confirmation', to: 'rents#rent_confirmation', as: 'rent_confirmation'
  get 'rents/confirmation_page'
  post 'rents/:address_id', to: 'rents#set_address', as: 'set_address'
  get 'rents/addressToRent'
  get 'games/myGames'
  resources :rent_games
  root to: 'games#index'
  resources :addresses
  get 'control_users/index'
  devise_for :users
  resources :rents
  resources :games
  resources :categories
  resource :carts, only: [:show]
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
