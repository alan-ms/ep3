require 'test_helper'

class RentGamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @rent_game = rent_games(:one)
  end

  test "should get index" do
    get rent_games_url
    assert_response :success
  end

  test "should get new" do
    get new_rent_game_url
    assert_response :success
  end

  test "should create rent_game" do
    assert_difference('RentGame.count') do
      post rent_games_url, params: { rent_game: { game_id: @rent_game.game_id, rent: @rent_game.rent, subtotal: @rent_game.subtotal, week: @rent_game.week } }
    end

    assert_redirected_to rent_game_url(RentGame.last)
  end

  test "should show rent_game" do
    get rent_game_url(@rent_game)
    assert_response :success
  end

  test "should get edit" do
    get edit_rent_game_url(@rent_game)
    assert_response :success
  end

  test "should update rent_game" do
    patch rent_game_url(@rent_game), params: { rent_game: { game_id: @rent_game.game_id, rent: @rent_game.rent, subtotal: @rent_game.subtotal, week: @rent_game.week } }
    assert_redirected_to rent_game_url(@rent_game)
  end

  test "should destroy rent_game" do
    assert_difference('RentGame.count', -1) do
      delete rent_game_url(@rent_game)
    end

    assert_redirected_to rent_games_url
  end
end
