require "application_system_test_case"

class RentGamesTest < ApplicationSystemTestCase
  setup do
    @rent_game = rent_games(:one)
  end

  test "visiting the index" do
    visit rent_games_url
    assert_selector "h1", text: "Rent Games"
  end

  test "creating a Rent game" do
    visit rent_games_url
    click_on "New Rent Game"

    fill_in "Game", with: @rent_game.game_id
    fill_in "Rent", with: @rent_game.rent
    fill_in "Subtotal", with: @rent_game.subtotal
    fill_in "Week", with: @rent_game.week
    click_on "Create Rent game"

    assert_text "Rent game was successfully created"
    click_on "Back"
  end

  test "updating a Rent game" do
    visit rent_games_url
    click_on "Edit", match: :first

    fill_in "Game", with: @rent_game.game_id
    fill_in "Rent", with: @rent_game.rent
    fill_in "Subtotal", with: @rent_game.subtotal
    fill_in "Week", with: @rent_game.week
    click_on "Update Rent game"

    assert_text "Rent game was successfully updated"
    click_on "Back"
  end

  test "destroying a Rent game" do
    visit rent_games_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Rent game was successfully destroyed"
  end
end
