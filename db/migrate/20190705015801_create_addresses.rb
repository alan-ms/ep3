class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.string :address
      t.string :district
      t.string :city
      t.string :cep
      t.string :complement
      t.boolean :visible, default: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
