class CreateRentGames < ActiveRecord::Migration[5.2]
  def change
    create_table :rent_games do |t|
      t.references :game, foreign_key: true
      t.references :rent
      t.integer :week
      t.float :subtotal

      t.timestamps
    end
  end
end
