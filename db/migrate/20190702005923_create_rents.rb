class CreateRents < ActiveRecord::Migration[5.2]
  def change
    create_table :rents do |t|
      t.float :totalValue
      t.string :status
      t.references :user, foreign_key: true
      t.belongs_to :address, index: true, null: true
      t.timestamps
    end
  end
end
